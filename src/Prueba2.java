import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Prueba2 {
	public static void main(String[] args) {

		String driver = "";
		String url = "";
		String user = "";
		String password = "";

		try {

			Properties p = new Properties();
			
			String nomArch = "src/config/configuration.properties";

			p.load(new FileInputStream(nomArch));
			driver = p.getProperty("driver");
			url = p.getProperty("url");
			user = p.getProperty("user");
			password = p.getProperty("password");
			
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);

			String consulta = "CREATE DATABASE Escuela";
			PreparedStatement pstmt = con.prepareStatement(consulta);
			pstmt.executeUpdate();
			pstmt.close();
			
			consulta = "CREATE TABLE Escuela.Personas (cedula INT NOT NULL PRIMARY KEY, nombre VARCHAR(45), apellido VARCHAR(45)) ";
			pstmt = con.prepareStatement(consulta);
			pstmt.executeUpdate();
			pstmt.close();
			
			consulta = "CREATE TABLE Escuela.Maestras (cedula INT NOT NULL PRIMARY KEY, grupo VARCHAR(45), FOREIGN KEY(cedula) REFERENCES Escuela.Personas(cedula)) ";
			pstmt = con.prepareStatement(consulta);
			pstmt.executeUpdate();
			pstmt.close();
			
			consulta = "CREATE TABLE Escuela.Alumnos (cedula INT NOT NULL PRIMARY KEY, cedulaMaestra INT, FOREIGN KEY(cedula) REFERENCES Escuela.Personas(cedula), "
					  +" FOREIGN KEY(cedulaMaestra) REFERENCES Escuela.Maestras(cedula))";
			pstmt = con.prepareStatement(consulta);
			pstmt.executeUpdate();
			pstmt.close();
			
			
			
			/*
			 * Personas (cedula INT, nombre VARCHAR(45), apellido VARCHAR(45)) 
				 Maestras (cedula INT, grupo VARCHAR(45)) 
				 Alumnos (cedula INT, cedulaMaestra INT)
			 */
			
			con.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
		e.printStackTrace();
		}

	}
}
