import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PruebaMaestra {
	public static void main(String[] args) {

		String driver = "";
		String url = "";
		String user = "";
		String password = "";

		try {

			Properties p = new Properties();
			
			String nomArch = "src/config/configuration.properties";

			p.load(new FileInputStream(nomArch));
			driver = p.getProperty("driver");
			url = p.getProperty("url");
			user = p.getProperty("user");
			password = p.getProperty("password");
			
			List<Integer> maestras = new ArrayList<Integer>();
			int cedulaMaestraConMasAlumnos=0;
			int cantidad=0;
			
			/* primer programa de prueba para ejemplo de acceso a MySQL desde Java */
			/*
			 * accede a una base de datos de MySQL llamada Prueba que contiene una tabla
			 * llamada Personas
			 */

			Class.forName(driver);

			Connection con = DriverManager.getConnection(url, user, password);

			Statement stmt = con.createStatement();
			String query = "SELECT m.cedula FROM maestras m ";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				maestras.add(rs.getInt("cedula"));
			}
			
			rs.close();
			for (Integer ced : maestras) {
				query = "SELECT m.cedula, COUNT(a.cedulaMaestra) cantidadAlumnos\r\n" + 
						"FROM alumnos a \r\n" + 
						"INNER JOIN maestras m ON m.cedula=a.cedulaMaestra\r\n" + 
						"WHERE m.cedula = ?";
				
				PreparedStatement pstmt = con.prepareStatement(query);
				pstmt.setInt (1, ced);
				
				rs = pstmt.executeQuery();
				
				if (rs.next()) {
					if(cantidad<rs.getInt("cantidadAlumnos"))
					{
						cedulaMaestraConMasAlumnos = rs.getInt("cedula");
						cantidad = rs.getInt("cantidadAlumnos");
					}
				}
			
				rs.close();
				pstmt.close();
			}
			
			query = "SELECT p.nombre, p.apellido \r\n" + 
					"FROM personas p \r\n" + 
					"WHERE p.cedula = ?";
			
			PreparedStatement pstmt = con.prepareStatement(query);
			pstmt.setInt (1, cedulaMaestraConMasAlumnos);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
					System.out.println("Nombre: " + rs.getString("nombre"));
					System.out.println("Apellido: " + rs.getString("apellido"));
			}
		
			pstmt.close();
			
			rs.close();
			stmt.close();

			/* 7. por ultimo, cierro la conexion con la base de datos */
			con.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
		e.printStackTrace();
		}

	}
}
