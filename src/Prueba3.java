import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class Prueba3 {
	public static void main(String[] args) {

		String driver = "";
		String url = "";
		String user = "";
		String password = "";

		try {

			Properties p = new Properties();
			
			String nomArch = "src/config/configuration.properties";

			p.load(new FileInputStream(nomArch));
			driver = p.getProperty("driver");
			url = p.getProperty("url");
			user = p.getProperty("user");
			password = p.getProperty("password");
			
			InputStreamReader is = new InputStreamReader (System.in); 
			BufferedReader br = new BufferedReader (is);
			
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);

			System.out.println("Ingrese comando: ");
			String consulta = br.readLine();
			
			while (!consulta.equals("exit"))
			{
				PreparedStatement pstmt = con.prepareStatement(consulta);
				try {
					int cant = pstmt.executeUpdate();
					System.out.println(cant + " filas afectadas");
				} catch (SQLException e) {
					System.out.println("Consulta incorrecta. "+e.getMessage());
				}
				finally {
					pstmt.close();
				}
				System.out.println("Ingrese comando: ");
				consulta = br.readLine();
			}
			
			con.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
		e.printStackTrace();
		}

	}
}
