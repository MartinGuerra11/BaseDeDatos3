import java.io.FileInputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PruebaEj5 {
	public static void main(String[] args) {

		String driver = "";
		String url = "";
		String user = "";
		String password = "";
		
		Connection con = null;

		try {

			Properties p = new Properties();
			
			String nomArch = "src/config/configuration.properties";

			p.load(new FileInputStream(nomArch));
			driver = p.getProperty("driver");
			url = p.getProperty("url");
			user = p.getProperty("user");
			password = p.getProperty("password");
			
			List<Integer> maestras = new ArrayList<Integer>();
			int cedulaMaestraConMasAlumnos=0;
			int cantidad=0;
			
			Class.forName(driver);

			con = DriverManager.getConnection(url, user, password);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			con.setAutoCommit(false);
			
			String sp = "{call BorrarMaestra (?)}";
			
			CallableStatement cstmt = con.prepareCall(sp);
			cstmt.setInt(1, 1234568);
			
			boolean isResultSet = cstmt.execute();
			if(isResultSet) {
				ResultSet rs = cstmt.getResultSet();
				while(rs.next()) {
					System.out.print("Cedula: ");
					System.out.println(rs.getInt("cedula"));
				}
				isResultSet = cstmt.getMoreResults();
				if(isResultSet) {
					rs = cstmt.getResultSet();
					if(rs.next()) {
						System.out.print("Grupo: ");
						System.out.println(rs.getInt("grupo"));
					}
					isResultSet = cstmt.getMoreResults();
					if(isResultSet) {
						rs = cstmt.getResultSet();
						if(rs.next()) {
							System.out.print("Nombre y Apellido: ");
							System.out.print(rs.getString("nombre"));
							System.out.println(" - "+ rs.getString("apellido"));
						}
						
					}
				}
				rs.close();
			}
			
			cstmt.close();
			
			con.commit();

			/* 7. por ultimo, cierro la conexion con la base de datos */
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			try {
				if(con!=null)
					con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
		e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}
